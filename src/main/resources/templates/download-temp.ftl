<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://unpkg.com/swiper/swiper-bundle.min.css">
    <script src="https://unpkg.com/swiper/swiper-bundle.min.js"> </script>
    <style>
        * {
            box-sizing: border-box;
        }
        html, body {
            width: 100%;
            height: 100%;
            margin: 0;
            padding: 0;
        }
        html {
            /* width 750px 下 1rem = 10px */
            font-size: calc(100vw / 75);
        }
        body {
            font-size: 1.6rem;
            color: #333;
        }
        .page-top {
            background-color: #365f93;
            color: #fff;
            font-size: 4.6rem;
            font-weight: 700;
            text-align: center;
            padding: 5rem 3.2rem;
        }
        .device-desc {
            font-size: 0.28rem;
            width: 100%;
            border-collapse: collapse;
        }
        .file-item {
            box-shadow: 0 2px 6px 1px rgba(0, 0, 0, .15);
            border-radius: 0.8rem;
            padding: 3.2rem;
            display: flex;
            line-height: 1.8;
        }
        .file-icon {
            width: 9.6rem;
            height: 12rem;
            background-image: url('https://static.clewm.net/cli/images/mina-cli/file_pdf@2x.png?v=20190905');
            background-size: 100% 100%;
            margin-right: 3.2rem;
        }
        .swiper-container {
            width: 100%;
            height: 50rem;
            border-radius: 4px;
            overflow: hidden;
        }
        .swiper-container .swiper-slide, .swiper-container .swiper-slide img {
            width: 100%;
            height: 100%;
        }
        .video-wrap {
            border-radius: 4px;
            overflow: hidden;
        }
        .video-wrap video {
            width: 100%;
        }
    </style>
</head>
<body>
<div>
    <div class="page-top">
        <div>螺杆式压缩机</div>
        <div>LJWLSD-03-0006</div>
        <img style="width: 100%;margin-top: 2rem;" src="https://ncstatic.clewm.net/rsrc/2020/1105/22/00fbf8221aba0e020227bc36f3d84a93.jpg?x-oss-process=image/resize,w_750/format,gif/sharpen,100/quality,Q_80/interlace,1/auto-orient,1" alt="">
    </div>
    <div style="padding: 5rem 3.2rem;">
        <table class="device-desc" border="1" cellpadding="8" borderColor="#e8e8e8">
            <thead><tr><th colspan="2" style="background-color: #f0f2f5;padding: 12px 0;">设备信息</th></tr></thead>
            <tbody>
            <tr><td>设备编号</td><td>LJWLSD-03-0006</td></tr>
            <tr><td>设备名称</td><td>螺杆式压缩机</td></tr>
            <tr><td>规格型号</td><td>6m³</td></tr>
            <tr><td>生产厂家</td><td>上海德哈螺杆压缩机有限公司</td></tr>
            <tr><td>出厂日期</td><td>2010.6.25</td></tr>
            <tr><td>责任人</td><td>林夕</td></tr>
            <tr><td>联系电话</td><td>19912340000</td></tr>
            </tbody>
        </table>
        <h3 style="font-size: 3.6rem;">操作说明书</h3>
        <div class="file-item">
            <div class="file-icon"></div>
            <div style="padding-top: 0.8rem;">
                <div style="font-size: 3.2rem;">空压机操作手册.pdf</div>
                <div style="font-size: 2.4rem;color: #9b9b9b;">513.09KB</div>
            </div>
        </div>
        <h3 style="font-size: 3.6rem;">设备图纸</h3>
        <div class="swiper-container">
            <div class="swiper-wrapper">
                <div class="swiper-slide"><img src="https://ncstatic.clewm.net/rsrc/2020/1105/22/4980ae0f95bdc9e185a48eba819c720d.jpeg?x-oss-process=image/resize,w_750/format,gif/sharpen,100/quality,Q_80/interlace,1/auto-orient,1" alt=""></div>
                <div class="swiper-slide"><img src="https://ncstatic.clewm.net/rsrc/2020/1105/22/feb9dbe2bfda1ee74ab60b9ddbc4ec32.png?x-oss-process=image/resize,w_515/format,gif/sharpen,100/quality,Q_80/interlace,1/auto-orient,1" alt=""></div>
                <div class="swiper-slide"><img src="https://ncstatic.clewm.net/rsrc/2020/1105/22/ce523c099e3f1ef4a86e55756f571e07.png?x-oss-process=image/resize,w_494/format,gif/sharpen,100/quality,Q_80/interlace,1/auto-orient,1" alt=""></div>
            </div>
            <div class="swiper-pagination"></div>
        </div>
        <h3 style="font-size: 3.6rem;">操作视频</h3>
        <div class="video-wrap">
            <video src="https://tcview-nc.clewm.net/3c23f7009ffe574107e941f14fab4949.H_25_4.mp4?sign=71c07ee8f884e20db2dc5d293aaa961f&amp;t=1604812162" playsinline x-webkit-airplay webkit-playsinline x5-playsinline x5-video-player-type="h5" x5-video-player-fullscreen="true" x5-video-orientation="landscape|portrait" controls="controls"></video>
        </div>
    </div>
</div>
<script>
    const mySwiper = new Swiper('.swiper-container', {
        loop: true,
        autoplay: true,
        pagination: {
            el: '.swiper-pagination',
        }
    })
</script>
</body>
</html>