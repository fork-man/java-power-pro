package com.st.power.pojo;

import com.st.power.pojo.common.BasePojo;
import lombok.Data;

/*
-- mysql
create table st_device(
id int(20) not null auto_increment,
deviceNo varchar(32),
deviceName varchar(32),
deviceCategoryId int(8),
deviceType varchar(32),
voltage int(3),
electricity int(3),
power int(3),
productLine varchar(32),
floorId int(8),
remark varchar(100),
primary key (`id`)
)

create table st_device_accessory_r(
id int(20) not null auto_increment,
deviceId int(20),
accessoryId int(20),
primary key (`id`)
);
alter table st_device_accessory_r add unique (deviceId,accessoryId);

create table st_device_spot_check_r(
id bigint(20) not null auto_increment,
deviceId bigint(20),
spotCheckId bigint(20),
unique(deviceId,spotCheckId),
primary key(`id`)
)
 */
@Data
public class DevicePojo extends BasePojo {
    private Long id;
    private String deviceNo;//设备编号
    private String deviceName;//设备名称
    private Integer deviceCategoryId;//设备类型，就是种类
    private String deviceCategoryName;//设备类型，就是种类
    private String deviceType;//设备型号，180*200
    private int voltage;//电压
    private int electricity;//电流
    private int power;//电功率
    private String productLine;//生产线
    private Integer floorId;//楼层，
    private String remark;
}
