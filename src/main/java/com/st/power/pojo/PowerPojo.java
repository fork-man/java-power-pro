package com.st.power.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
/*
-- sql-server
create table st_power(
id int,
ammeterNo int,
time datetime,
numI decimal(10,1),
numV decimal(10,1),
numPowerRate decimal(10,1),
numPowerCurrentTime decimal(10,1)
)

-- mysql
create table st_power(
id int(20) not null auto_increment,
ammeterNo int(8),
time datetime,
numI decimal(10,1),
numV decimal(10,1),
numPowerRate decimal(10,1),
numPowerCurrentTime decimal(10,1),
primary key (`id`)
)

 */

public class PowerPojo {
    private Long id;
    private int ammeterNo;
    @JsonFormat(timezone = "GMT+8", pattern = "YYYY-MM-dd hh:mm:ss")
    private Date time;

    private String ammeterName;//名字
    private String groupName;

    private BigDecimal numI;//电流
    private BigDecimal numV;//电压
    private BigDecimal numPowerRate;//额定电能
    private BigDecimal numPowerCurrentTime;//实时电能


}
