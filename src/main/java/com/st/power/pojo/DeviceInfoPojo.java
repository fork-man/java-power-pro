package com.st.power.pojo;

import com.st.power.pojo.common.BasePojo;
import lombok.Data;

import java.math.BigDecimal;

/*
create table st_device_info(
    id int(20) not null auto_increment,
    deviceId int(20),
    runningTime decimal(10,3),
    grandTotalRunningTime decimal(10,3),
    maintainTime int(6),
    deviceStatus int(2),
    spotCheckCycle int(6),
    primary key(`id`)
)
alter table st_device_info add unique (deviceId);

 */
@Data
public class DeviceInfoPojo extends BasePojo {
    private Long id;
    private Long deviceId;
    private String deviceNo;
    private String deviceName;
    private BigDecimal runningTime;//当期运行
    private BigDecimal grandTotalRunningTime;//累计运行
    private Integer maintainTime;//维护周期
    private Integer deviceStatus;//设备状态
    private Integer spotCheckCycle;//点检周期
}
