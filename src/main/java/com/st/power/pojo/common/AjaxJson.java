package com.st.power.pojo.common;

import java.util.List;

public class AjaxJson<T> {
    static final int ok = 1;
    public int code = ok;
    public List<T> list;
    public int count;
    public T data;
    public String msg;
    public Object extraData;
}
