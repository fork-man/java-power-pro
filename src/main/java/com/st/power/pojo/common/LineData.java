package com.st.power.pojo.common;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class LineData {
    List<String> title;
    List<BigDecimal> pointData;
}
