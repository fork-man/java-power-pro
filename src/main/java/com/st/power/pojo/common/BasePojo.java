package com.st.power.pojo.common;

import lombok.Data;

@Data
public class BasePojo {
    private int rowIndex;
    private int pageSize;

    //前端接过来的东西
    private int page;
    private int limit;

    //就专指创建时间
    private String startTime;
    private String endTime;

    public int getRowIndex() {
        return (page - 1) * limit;
    }

    public int getPageSize() {
        return limit;
    }
}
