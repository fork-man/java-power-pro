package com.st.power.pojo.common;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

@Data
public class PieData {
    List<String> title;
    List<PieInner> pointData;

    public static class PieInner {
        public String name;
        public BigDecimal value;
    }
}
