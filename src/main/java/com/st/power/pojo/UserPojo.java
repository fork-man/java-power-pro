package com.st.power.pojo;

import lombok.Data;

@Data
public class UserPojo {
    private Long id;
    private String name;
    private String password;
    private String role;//普通人员，和管理员，集团人员
    private int perms;//权限，所属工厂数据的权限
    private String permissionName;
}
