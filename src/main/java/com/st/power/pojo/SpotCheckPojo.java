package com.st.power.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.st.power.pojo.common.BasePojo;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/*
-- mysql
create table st_spot_check(
id int(20) not null auto_increment,
spotCheckNo varchar(32),
spotCheckName varchar(32),
createTime datetime,
primary key(`id`)
);
alter table st_spot_check add unique (spotCheckNo);
 */
@Data
public class SpotCheckPojo extends BasePojo {
    private Long id;
    private String spotCheckNo;//
    private String spotCheckName;//
    @JsonFormat(timezone = "GMT+8", pattern = "YYYY-MM-dd hh:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
