package com.st.power.pojo;

import lombok.Data;

/*
create table st_device_statistics(
id bigint(20) not null auto_increment,
deviceNo varchar(32),
deviceName varchar(32),
operateType int(2),
operateDuring int(60),
key
)
 */
@Data
public class DeviceStatistics {
    private Long id;
    private String deviceNo;
    private String deviceName;
    private Integer operateType;
    private int operateDuring;
    private int totalNum;
}
