package com.st.power.pojo;

import com.st.power.pojo.common.BasePojo;
import lombok.Data;

/*
-- mysql
create table st_accessory(
    id int(20) not null auto_increment,
    accessoryNo varchar(32),

    accessoryName varchar(32),
    accessoryCategory int(20),
    accessoryType varchar(32),
    accessoryMaker varchar(32),
    remark varchar(32),
    primary key(`id`)
)
 */
@Data
public class AccessoryPojo extends BasePojo {
    private Long id;
    private String accessoryNo;//配件编号
    private int accessoryCategory;//配件类型
    private String accessoryType;//配件型号
    private String accessoryName;//配件名称
    private String accessoryMaker;//生产厂家
    private String remark;//备注
}
