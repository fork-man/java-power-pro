package com.st.power.pojo;

import lombok.Data;
/*
-- mysql
create table st_accessory_category(
id int(20) not null auto_increment,
name varchar(32),
primary key(`id`)
)
alter table st_accessory_category add unique (name);
 */
@Data
public class AccessoryCategoryPojo {
    private long id;
    private String name;
}
