package com.st.power.pojo;

import lombok.Data;

import java.util.Date;

@Data
public class PowerParams {
    int page=1;
    int size=5;
    int ammeterNo=1;
    String startTime="2020-10-22 09:55:00";
    String endTime="2020-11-22 09:55:00";
}
