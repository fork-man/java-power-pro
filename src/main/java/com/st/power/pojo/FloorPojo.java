package com.st.power.pojo;

import lombok.Data;

/*
-- mysql
create table st_floor(
    id int(20) not null auto_increment,
    name varchar(32),
    primary key(`id`)
);
alter table st_floor add unique (`name`);
 */
@Data
public class FloorPojo {
    private Long id;
    private String name;
}
