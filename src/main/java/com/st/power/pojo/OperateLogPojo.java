package com.st.power.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.st.power.pojo.common.BasePojo;
import lombok.Data;

import java.util.Date;

/*
create table st_operate_log(
id bigint(20) not null auto_increment,
deviceId bigint(20),
type int(2),
operateUser varchar(32),
during int(3),
createTime datetime,
primary key(`id`)
)
 */
@Data
public class OperateLogPojo extends BasePojo {
    private Long id;
    private Long deviceId;

    private String deviceNo;
    private String deviceName;

    private Integer type;
    private String operateUser;//操作员
    private int during;//操作用时
    @JsonFormat(timezone = "GMT+8", pattern = "YYYY-MM-dd hh:mm:ss")
    private Date createTime;//操作时间
}
