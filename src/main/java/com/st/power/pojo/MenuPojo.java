package com.st.power.pojo;

import lombok.Data;

import java.util.List;

@Data
public class MenuPojo {

    private Long id;
    private String name;
    private List<MenuPojo> children;
}
