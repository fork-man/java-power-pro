package com.st.power.pojo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;

import java.util.List;

@Data
/*
    -- 车间-组-表，三层结构,组织结构
    create table st_3_level(
        id int,
        name varchar(50),
        type int,
        parentId int
    )
    -- mysql
    create table st_3_level(
        id int(20) not null auto_increment,
        name varchar(50),
        type int(4),
        parentId int(20),
        primary key (`id`)
    )
 */
@JsonInclude(JsonInclude.Include.NON_NULL)
public class St3Level {
    private int id;
    private String name;
    /**
     * 10,车间;
     * 20,组;
     * 30,表;
     */
    private Integer ammeterNo;
    private int type;
    private int parentId;

    private List<St3Level> children;
}
