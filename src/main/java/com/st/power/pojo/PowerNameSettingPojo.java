package com.st.power.pojo;

import lombok.Data;

@Data
/*
-- sql-server
create table st_power_name(
id int,
ammeterNo int,
totalAmmeter int,
ammeterName varchar(50),
mGroup varchar(50),
mRoom varchar(50),
remark varchar(50)
)

-- mysql
create table st_power_name(
id int(20) not null auto_increment,
ammeterNo int(8),
totalAmmeter int(2),
ammeterName varchar(50),
mGroup varchar(50),
mRoom varchar(50),
remark varchar(50),
primary key (`id`)
)

 */
public class PowerNameSettingPojo {
    private Integer id;
    private int ammeterNo;
    private String ammeterName;

    private int mRoom;//车间
    private int mGroup;//组名

    private String mRoomName;
    private String mGroupName;

    private String remark;//备注
    private int totalAmmeter;//是总表
    private int createOrUpdate;

}
