package com.st.power.pojo;

import lombok.Data;
/*
-- mysql
create table st_device_category(
id int(20) not null auto_increment,
name varchar(32),
primary key (`id`)
)
    alter table st_device add unique (name);
 */
@Data
public class DeviceCategoryPojo {
    private Long id;
    private String name;
}
