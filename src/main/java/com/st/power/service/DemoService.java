package com.st.power.service;

/**
 * @author xiaosuda
 * @date 2017/12/14
 */
public interface DemoService {

    String sayHello(String name);
}
