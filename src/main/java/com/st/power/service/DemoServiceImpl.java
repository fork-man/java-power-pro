package com.st.power.service;

import org.springframework.stereotype.Service;

/**
 * @author xiaosuda
 * @date 2017/12/14
 */
@Service
public class DemoServiceImpl implements DemoService {
    //https://blog.csdn.net/su20145104009/article/details/78842185
    @Override
    public String sayHello(String name) {
        return "你说：" + name;
    }
}
