package com.st.power.task;

import com.st.power.mapper.PowerMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.PowerPojo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Random;

@Component
@Slf4j
public class FakePowerTask {
    @Autowired
    PowerMapper powerMapper;

//    @Scheduled(cron = "*/60 * * * * ?")
    public void execute() {
        List<PowerPojo> list = new ArrayList<>();
        Random random = new Random();
//        for (int i = 0; i < 60; i++) {
        int ran = random.nextInt(3);
        power += fakePower1[ran];
        int mI = fakeI[ran];
        power2 += fakePower2[ran];
        PowerPojo powerPojo = new PowerPojo();
        powerPojo.setAmmeterNo(21);
        powerPojo.setAmmeterName("电表1");
        powerPojo.setNumI(new BigDecimal(mI + ""));
        powerPojo.setNumV(new BigDecimal(mI + ""));
        powerPojo.setNumPowerRate(new BigDecimal(power + ""));
        powerPojo.setNumPowerCurrentTime(new BigDecimal(power + ""));
        Date time = new Date();
//            powerPojo.setTime(new Date(time.getTime() + i * 60 * 1000));
        powerPojo.setTime(time);
        list.add(powerPojo);
//        }
        powerMapper.savePowerList(list);

        PowerPojo powerPojo2 = new PowerPojo();
        powerPojo2.setAmmeterNo(12);
        powerPojo2.setNumI(new BigDecimal(mI + ""));
        powerPojo2.setNumV(new BigDecimal(mI + ""));
        powerPojo2.setNumPowerRate(new BigDecimal(power2 + ""));
        powerPojo2.setNumPowerCurrentTime(new BigDecimal(power2 + ""));
        powerPojo2.setTime(time);
        list.clear();
        list.add(powerPojo2);
        powerMapper.savePowerList(list);

        PowerPojo powerPojo3 = new PowerPojo();
        powerPojo3.setAmmeterNo(11);
        powerPojo3.setNumI(new BigDecimal(mI + ""));
        powerPojo3.setNumV(new BigDecimal(mI + ""));
        powerPojo3.setNumPowerRate(new BigDecimal(power + ""));
        powerPojo3.setNumPowerCurrentTime(new BigDecimal(power + ""));
        powerPojo3.setTime(time);
        list.clear();
        list.add(powerPojo3);
        powerMapper.savePowerList(list);
    }

    @RequestMapping("savePowerList")
    public AjaxJson savePowerList() {
        AjaxJson<String> ajaxJson = new AjaxJson<>();

        return ajaxJson;
    }

    static int[] fakePower1 = {10, 20, 30};
    static int[] fakePower2 = {30, 20, 40};
    static int[] fakeI = {5, 4, 6};
    static int power = 400;
    static int power2 = 200;
}
