package com.st.power.mapper;

import com.st.power.controller.AccessoryController;
import com.st.power.pojo.AccessoryCategoryPojo;
import com.st.power.pojo.AccessoryPojo;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public interface AccessoryMapper {
    List<AccessoryPojo> getAccessoryList(AccessoryPojo accessoryPojo);

    Long accessoryCount();

    List<AccessoryPojo> getRelationship(Long id);

    List<AccessoryPojo> accessoryNotAddList(Long id);

    int saveAccessory(AccessoryPojo accessoryPojo);

    int updateAccessory(AccessoryPojo accessoryPojo);

    List<AccessoryCategoryPojo> accessoryCategoryList();

    int saveAccessoryCategory(AccessoryCategoryPojo accessoryCategoryPojo);

    int saveRelationship(List<AccessoryController.Relationship> relationshipList);

}
