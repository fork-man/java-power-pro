package com.st.power.mapper;

import com.st.power.pojo.PowerNameSettingPojo;
import com.st.power.pojo.PowerParams;
import com.st.power.pojo.PowerPojo;
import com.st.power.pojo.St3Level;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PowerMapper {
    List<PowerPojo> powerList(PowerParams powerParams);

    Long powerListCount(PowerParams powerParams);

    List<PowerNameSettingPojo> powerNameList();

    Long savePowerName(PowerNameSettingPojo powerNameSettingPojo);

    Long updatePowerName(PowerNameSettingPojo powerNameSettingPojo);

    PowerNameSettingPojo findPowerNameByNo(PowerNameSettingPojo powerNameSettingPojo);

    Long savePowerList(List<PowerPojo> list);

    PowerPojo powerChoose();
    List<PowerPojo> powerListByFactory(@Param("id")Integer id,@Param("method") int method,@Param("startTime") String startTime,@Param("endTime") String endTime);
    List<PowerPojo> powerTotalList(@Param("id")Integer id,@Param("startTime") String startTime,@Param("endTime") String endTime);
}
