package com.st.power.mapper;

import com.st.power.pojo.St3Level;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrganizationMapper {
    int saveLevel(St3Level level);

    List<St3Level> getLevel();

    List<St3Level> getFactory();

    List<Long> getIds();

    List<St3Level> roomOrGroup(Integer id);
}
