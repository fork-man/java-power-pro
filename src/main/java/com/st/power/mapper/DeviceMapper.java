package com.st.power.mapper;

import com.st.power.pojo.DeviceCategoryPojo;
import com.st.power.pojo.DevicePojo;
import com.st.power.pojo.FloorPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceMapper {
    List<DevicePojo> getDeviceList(DevicePojo devicePojo);

    Long deviceCount();

    Integer saveDevice(DevicePojo devicePojo);

    Integer updateDevice(DevicePojo devicePojo);

    List<DeviceCategoryPojo> categoryList();

    int saveCategory(DeviceCategoryPojo deviceCategoryPojo);

    List<FloorPojo> floorList();

    int saveFloor(FloorPojo floorPojo);
}
