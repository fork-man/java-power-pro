package com.st.power.mapper;

import com.st.power.controller.SpotCheckController;
import com.st.power.pojo.SpotCheckPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SpotCheckMapper {
    int saveSpotCheck(SpotCheckPojo spotCheckPojo);

    int updateSpotCheck(SpotCheckPojo spotCheckPojo);

    List<SpotCheckPojo> spotCheckList(SpotCheckPojo spotCheckPojo);
    Long spotCheckCount();

    List<SpotCheckPojo> getRelationship(Long id);

    List<SpotCheckPojo> spotCheckNotAddList(Long id);

    int saveRelationship(List<SpotCheckController.Relationship> list);
}
