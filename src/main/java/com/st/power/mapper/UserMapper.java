package com.st.power.mapper;

import com.st.power.pojo.UserPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserMapper {
    List<UserPojo> getUserList(UserPojo userPojo);

    long saveUser(UserPojo userPojo);

    long updateUser(UserPojo userPojo);
}
