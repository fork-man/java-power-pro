package com.st.power.mapper;

import com.st.power.pojo.DeviceStatistics;
import com.st.power.pojo.OperateLogPojo;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OperateLogMapper {
    List<OperateLogPojo> operateLogList(OperateLogPojo operateLogPojo);

    Long countOperateLog(OperateLogPojo operateLogPojo);

    List<DeviceStatistics> operateLogStatisticList(OperateLogPojo operateLogPojo);

    Long countOperateLogStatistic(OperateLogPojo operateLogPojo);

    int saveOperate(OperateLogPojo operateLogPojo);
}
