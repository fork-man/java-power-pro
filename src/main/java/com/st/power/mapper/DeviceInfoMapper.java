package com.st.power.mapper;

import com.st.power.pojo.DeviceInfoPojo;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DeviceInfoMapper {
    List<DeviceInfoPojo> deviceInfoList(DeviceInfoPojo deviceInfoPojo);

    Long deviceInfoCount(DeviceInfoPojo deviceInfoPojo);

    int saveDeviceInfo(DeviceInfoPojo deviceInfoPojo);

    int updateDeviceInfo(DeviceInfoPojo deviceInfoPojo);
}
