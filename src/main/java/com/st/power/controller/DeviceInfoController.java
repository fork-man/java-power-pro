package com.st.power.controller;

import com.st.power.mapper.DeviceInfoMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.DeviceInfoPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("device-info")
public class DeviceInfoController {
    @Autowired
    DeviceInfoMapper deviceInfoMapper;

    @RequestMapping("deviceInfoList")
    public AjaxJson deviceInfoList(DeviceInfoPojo deviceInfoPojo) {
        AjaxJson<DeviceInfoPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = deviceInfoMapper.deviceInfoList(deviceInfoPojo);
        ajaxJson.count = deviceInfoMapper.deviceInfoCount(deviceInfoPojo).intValue();
        return ajaxJson;
    }

    @RequestMapping("saveDeviceInfo")
    public AjaxJson saveDeviceInfo(DeviceInfoPojo deviceInfoPojo) {
        AjaxJson<DeviceInfoPojo> ajaxJson = new AjaxJson<>();
        Long id = deviceInfoPojo.getId();
        if (null == id) deviceInfoMapper.saveDeviceInfo(deviceInfoPojo);
        else deviceInfoMapper.updateDeviceInfo(deviceInfoPojo);
        ajaxJson.data = deviceInfoPojo;
        return ajaxJson;
    }

}
