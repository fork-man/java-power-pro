package com.st.power.controller;

import com.st.power.mapper.DeviceMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.DeviceCategoryPojo;
import com.st.power.pojo.DevicePojo;
import com.st.power.pojo.FloorPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("device")
public class DeviceController {
    @Autowired
    DeviceMapper deviceMapper;

    @RequestMapping("getDeviceList")
    public AjaxJson getDeviceList(DevicePojo devicePojo) {
        AjaxJson<DevicePojo> ajaxJson = new AjaxJson<>();
        List<DevicePojo> deviceList = deviceMapper.getDeviceList(devicePojo);
        ajaxJson.list = deviceList;
        ajaxJson.count = deviceMapper.deviceCount().intValue();
        return ajaxJson;
    }

    @RequestMapping("saveDevice")
    public AjaxJson saveDevice(DevicePojo devicePojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        deviceMapper.saveDevice(devicePojo);
        return ajaxJson;
    }

    @RequestMapping("updateDevice")
    public AjaxJson updateDevice(DevicePojo devicePojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        deviceMapper.updateDevice(devicePojo);
        return ajaxJson;
    }

    //-----------------------------------------------------------------------------------------------------------------

    @RequestMapping("saveCategory")
    public AjaxJson saveCategory(DeviceCategoryPojo deviceCategoryPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        deviceMapper.saveCategory(deviceCategoryPojo);
        return ajaxJson;
    }

    @RequestMapping("categoryList")
    public AjaxJson categoryList() {
        AjaxJson<DeviceCategoryPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = deviceMapper.categoryList();
        return ajaxJson;
    }

    //-----------------------------------------------------------------------------------------------------------------
    @RequestMapping("floorList")
    public AjaxJson floorList() {
        AjaxJson<FloorPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = deviceMapper.floorList();
        return ajaxJson;
    }

    @RequestMapping("saveFloor")
    public AjaxJson saveFloor(FloorPojo floorPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        deviceMapper.saveFloor(floorPojo);
        return ajaxJson;
    }
}
