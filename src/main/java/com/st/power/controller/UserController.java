package com.st.power.controller;

import com.st.power.mapper.UserMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.UserPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    UserMapper userMapper;

    @RequestMapping("userList")
    public AjaxJson<UserPojo> userList() {
        AjaxJson<UserPojo> ajaxJson = new AjaxJson<>();
        List<UserPojo> userList = userMapper.getUserList(null);
        ajaxJson.list = userList;
        return ajaxJson;
    }

    @RequestMapping("saveUser")
    public AjaxJson saveUser(UserPojo userPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        userMapper.saveUser(userPojo);
        return ajaxJson;
    }

    @RequestMapping("updateUser")
    public AjaxJson updateUser(UserPojo userPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        List<UserPojo> userList = userMapper.getUserList(userPojo);
        if (userList.size() != 0 && !userPojo.getId().equals(userList.get(0).getId())) {
            ajaxJson.code = -1;
            return ajaxJson;
        }
        userMapper.updateUser(userPojo);
        return ajaxJson;
    }
}
