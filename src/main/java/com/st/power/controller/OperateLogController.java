package com.st.power.controller;

import com.st.power.mapper.OperateLogMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.DeviceStatistics;
import com.st.power.pojo.OperateLogPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.Random;

@RestController
@RequestMapping("operate-log")
public class OperateLogController {
    @Autowired
    OperateLogMapper operateLogMapper;

    @RequestMapping("operateLogList")
    public AjaxJson operateLogList(OperateLogPojo operateLogPojo) {
        AjaxJson<OperateLogPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = operateLogMapper.operateLogList(operateLogPojo);
        ajaxJson.count = operateLogMapper.countOperateLog(operateLogPojo).intValue();
        return ajaxJson;
    }

    @RequestMapping("operateRandom")
    public AjaxJson operateRandom() {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        Random random = new Random();
        OperateLogPojo operateLogPojo = new OperateLogPojo();
        operateLogPojo.setCreateTime(new Date());
        operateLogPojo.setType(2);
        operateLogPojo.setOperateUser("王五");
        operateLogPojo.setDuring(3);
        for (int j = 0; j < 100; j++)
            for (int i = 2; i < 11; i++) {
                operateLogPojo.setDeviceId((long) i);
                int ran = random.nextInt(2);

                if (1 == ran) operateLogMapper.saveOperate(operateLogPojo);
            }
        return ajaxJson;
    }

    @RequestMapping("operateLogStatisticList")
    public AjaxJson operateLogStatisticList(OperateLogPojo operateLogPojo) {
        AjaxJson<DeviceStatistics> ajaxJson = new AjaxJson<>();
        ajaxJson.list = operateLogMapper.operateLogStatisticList(operateLogPojo);
        ajaxJson.count = operateLogMapper.countOperateLogStatistic(operateLogPojo).intValue();
        return ajaxJson;
    }
}
