package com.st.power.controller;

import com.st.power.mapper.PowerMapper;
import com.st.power.pojo.*;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.common.LineData;
import com.st.power.pojo.common.PieData;
import org.apache.commons.collections4.MultiValuedMap;
import org.apache.commons.collections4.multimap.ArrayListValuedHashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("power")
public class PowerController {
    @Autowired
    PowerMapper powerMapper;

    @RequestMapping("powerList")
    public AjaxJson<PowerPojo> powerList(PowerParams powerParams) {
        AjaxJson<PowerPojo> ajaxJson = new AjaxJson<>();
        int page = powerParams.getPage();
        powerParams.setPage(page - 1);
        List<PowerPojo> list = powerMapper.powerList(powerParams);
        Long count = powerMapper.powerListCount(powerParams);
        ajaxJson.list = list;
        ajaxJson.count = count.intValue();
        ajaxJson.extraData = processList(list);
        return ajaxJson;
    }

    static LineData processList(List<PowerPojo> list) {

        BigDecimal last = null;
        List<String> titleList = new ArrayList<>();
        List<BigDecimal> dataList = new ArrayList<>();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        for (int i = 0; i < list.size(); i++) {
            PowerPojo powerPojo = list.get(i);
            BigDecimal numPowerRate = powerPojo.getNumPowerRate();
            Date time = powerPojo.getTime();
            if (null != last) {
                BigDecimal res = numPowerRate.subtract(last);
                if(res.signum()>0) {
                    dataList.add(res);
                    String tStr = sdf.format(time);
                    titleList.add(tStr);
                }
            }
            last = numPowerRate;
        }
        LineData lineData = new LineData();
        lineData.setTitle(titleList);
        lineData.setPointData(dataList);
        return lineData;
    }

    @RequestMapping("homeChart")
    public AjaxJson homeChart(Integer id, String startTime, String endTime) {
        AjaxJson<Map<String, Object>> ajaxJson = new AjaxJson<>();
        List<PowerPojo> minList = powerMapper.powerListByFactory(id, 1, startTime, endTime);
        List<PowerPojo> maxList = powerMapper.powerListByFactory(id, 2, startTime, endTime);
        List<PowerPojo> totalList = powerMapper.powerTotalList(id, startTime, endTime);

        MultiValuedMap<String, BigDecimal> multiMap = new ArrayListValuedHashMap<>();
//        pieData.
        List<String> titleList = new ArrayList<>();
        if (minList.size() > maxList.size()) {
            ajaxJson.code = -1;
            return ajaxJson;
        }
        List<PieData.PieInner> pointList = new ArrayList<>();
        for (int i = 0; i < minList.size(); i++) {
            PowerPojo minPower = minList.get(i);
            PowerPojo maxPower = maxList.get(i);
            String minName = minPower.getGroupName();
            String maxName = maxPower.getGroupName();
            BigDecimal minNumPowerCurrentTime = minPower.getNumPowerCurrentTime();
            BigDecimal maxNumPowerCurrentTime = maxPower.getNumPowerCurrentTime();
            multiMap.put(minName, minNumPowerCurrentTime);
            multiMap.put(maxName, maxNumPowerCurrentTime);
        }
        Set<String> keySet = multiMap.keySet();
        for (String name : keySet) {
            Collection<BigDecimal> list = multiMap.get(name);
            int size = list.size();

            if (size > 2) {
                //todo 发送到 elk
                ajaxJson.code = -1;
                return ajaxJson;
            }
            List<BigDecimal> aList = ((List<BigDecimal>) list);
            BigDecimal min = aList.get(0);
            if (2 != size) continue;
            BigDecimal max = aList.get(1);
            titleList.add(name);

            PieData.PieInner inner = new PieData.PieInner();
            inner.name = name;
            BigDecimal value = max.subtract(min);
            if (value.signum() < 0) {
                //todo 发送到 elk
            }
            inner.value = value.abs();
            pointList.add(inner);
        }

        PieData pieData = new PieData();
        pieData.setTitle(titleList);
        pieData.setPointData(pointList);
        HashMap<String, Object> map = new HashMap<>();
        map.put("pieData", pieData);

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        List<String> lineTitleList = new ArrayList<>();
        List<BigDecimal> linePointData = new ArrayList<>();
        for (PowerPojo powerPojo : totalList) {
            BigDecimal numPowerCurrentTime = powerPojo.getNumPowerCurrentTime();
            Date time = powerPojo.getTime();
            lineTitleList.add(sdf.format(time));
            linePointData.add(numPowerCurrentTime);
        }
        LineData lineData = new LineData();
        lineData.setTitle(lineTitleList);
        lineData.setPointData(linePointData);
        map.put("lineData", lineData);

        ajaxJson.data = map;
        return ajaxJson;
    }

    @RequestMapping("powerNameList")
    public AjaxJson<PowerNameSettingPojo> powerNameList() {
        AjaxJson<PowerNameSettingPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = powerMapper.powerNameList();
        return ajaxJson;
    }

    @RequestMapping("savePowerName")
    public AjaxJson<String> savePowerName(PowerNameSettingPojo pojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        if (pojo.getCreateOrUpdate() == 1) {

            powerMapper.updatePowerName(pojo);
            return ajaxJson;
        }

        PowerNameSettingPojo powerNameByNo = powerMapper.findPowerNameByNo(pojo);
        if (powerNameByNo != null) {
            ajaxJson.code = -1;
            return ajaxJson;//不保存直接退出
        }
        powerMapper.savePowerName(pojo);
        return ajaxJson;
    }

//    @RequestMapping("menuList")
//    public AjaxJson<MenuPojo> menuList() {
//        AjaxJson<MenuPojo> ajaxJson = new AjaxJson<>();
//        List<MenuPojo> list = new ArrayList<>();
//        ajaxJson.list = list;
//        MenuPojo pojo = new MenuPojo();
//        pojo.setName("车间1");
//        list.add(pojo);
//
//        List<MenuPojo> list1 = new ArrayList<>();
//        MenuPojo pojo1 = new MenuPojo();
//        pojo1.setName("组1");
//        list1.add(pojo1);
//        pojo.setChildren(list1);
//
//        List<MenuPojo> list2 = new ArrayList<>();
//        MenuPojo pojo2 = new MenuPojo();
//        pojo2.setName("设备1");
//        list2.add(pojo2);
//        pojo1.setChildren(list2);
//
//        return ajaxJson;
//    }



//    public static void main(String[] args) {
//        StringBuilder sb = new StringBuilder();
//        Random random = new Random();
//        for (int i = 0; i < 10; i++) {
//            int ran=random.nextInt(3);
//            power+=fakePower1[ran];
//            int mI=fakeI[ran];
//            sb.append(String.format("%d:%d\n",power,mI));
//        }
//        System.out.println(sb.toString());
//    }
}
