package com.st.power.controller;

import com.st.power.mapper.SpotCheckMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.SpotCheckPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping("spot-check")
public class SpotCheckController {
    @Autowired
    SpotCheckMapper spotCheckMapper;

    @RequestMapping("spotCheckList")
    public AjaxJson spotCheckList(SpotCheckPojo spotCheckPojo) {
        AjaxJson<SpotCheckPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = spotCheckMapper.spotCheckList(spotCheckPojo);
        Long count = spotCheckMapper.spotCheckCount();
        ajaxJson.count = count.intValue();
        return ajaxJson;
    }

    @RequestMapping("saveSpotCheck")
    public AjaxJson saveSpotCheck(SpotCheckPojo spotCheckPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        spotCheckPojo.setCreateTime(new Date());
        spotCheckMapper.saveSpotCheck(spotCheckPojo);
        return ajaxJson;
    }

    @RequestMapping("updateSpotCheck")
    public AjaxJson updateSpotCheck(SpotCheckPojo spotCheckPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        spotCheckMapper.updateSpotCheck(spotCheckPojo);
        return ajaxJson;
    }

    //--------------------------------------------------------------------------------------------------
    @RequestMapping("getRelationship")
    public AjaxJson getRelationship(Long id) {
        AjaxJson<SpotCheckPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = spotCheckMapper.getRelationship(id);
        return ajaxJson;
    }

    @RequestMapping("spotCheckNotAddList")
    public AjaxJson spotCheckNotAddList(Long id) {
        AjaxJson<SpotCheckPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = spotCheckMapper.spotCheckNotAddList(id);
        return ajaxJson;
    }

    @RequestMapping("saveRelationship")
    public AjaxJson saveRelationship(Long deviceId, String spotCheckIdArr) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        String[] arr = spotCheckIdArr.split(",");
        List<Relationship> list = new ArrayList<>();
        for (String id : arr) {
            Relationship relationship = new Relationship();
            relationship.deviceId = deviceId;
            relationship.spotCheckId = Long.valueOf(id);
            list.add(relationship);
        }
        spotCheckMapper.saveRelationship(list);
        return ajaxJson;
    }

    public static class Relationship {
        public Long deviceId;
        public Long spotCheckId;
    }
}
