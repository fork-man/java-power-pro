package com.st.power.controller;

import com.st.power.mapper.PowerMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.PowerPojo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.text.SimpleDateFormat;
import java.util.*;

@RestController
@RequestMapping("util")
public class UtilController {
    @Autowired
    PowerMapper powerMapper;

    @RequestMapping("powerTime")
    public AjaxJson powerTime() {
        AjaxJson<Map<String, Object>> ajaxJson = new AjaxJson<>();
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        Date d = cal.getTime();

        SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
        String yesterday = sp.format(d);
        String today = sp.format(new Date());
        HashMap<String, Object> map = new HashMap<>();
        map.put("yesterday", yesterday + " 00:00:00");
        map.put("today", today + " 23:59:59");
        PowerPojo powerPojo = powerMapper.powerChoose();
        int ammeterNo = powerPojo.getAmmeterNo();
        map.put("ammeterNo", ammeterNo);
        ajaxJson.data = map;
        return ajaxJson;
    }

    @RequestMapping("powerTimeSection")
    public AjaxJson powerTimeSection() {
        AjaxJson<Map<String, Object>> ajaxJson = new AjaxJson<>();
        HashMap<String, Object> map = new HashMap<>();
//        map.put("lastMonday", getPreviousMonday() + " 00:00:00");
//        map.put("lastSunday", getPreviousWeekSunday() + " 23:59:59");

        SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String today = sp.format(date);
        map.put("thisMonday", sp.format(getWeekStartDate()) + " 00:00:00");
        map.put("todayEnd", today + " 23:59:59");

//        map.put("monthFirstDay", monthFirstDay() + " 00:00:00");
//
//        map.put("yearFirstDay", yearFirstDay() + " 00:00:00");
        map.put("before15Min", before15Min() + ":00");
        map.put("before1H", before1H() + ":00");
        map.put("today0Time", today + " 00:00:00");
        ajaxJson.data = map;
        return ajaxJson;
    }

    public static String before15Min() {
        //15分钟前
        Date date = new Date();
        Date before15Min = new Date(date.getTime() - 15 * 60 * 1000);
        SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sp.format(before15Min);
    }

    public static String before1H() {
        Date date = new Date();
        //1小时前
        Date before1H = new Date(date.getTime() - 60 * 60 * 1000);
        SimpleDateFormat sp = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        return sp.format(before1H);
    }

    public static String monthFirstDay() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstday, lastday;
        // 获取前月的第一天
        Calendar cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 0);
        cale.set(Calendar.DAY_OF_MONTH, 1);
        firstday = format.format(cale.getTime());
        // 获取前月的最后一天
        cale = Calendar.getInstance();
        cale.add(Calendar.MONTH, 1);
        cale.set(Calendar.DAY_OF_MONTH, 0);
        lastday = format.format(cale.getTime());
//        System.out.println("本月第一天和最后一天分别是 ： " + firstday + " and " + lastday);
        return firstday;
    }

    public static String yearFirstDay() {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        String firstday;
        // 获取前月的第一天
        Calendar cale = Calendar.getInstance();
        cale.add(Calendar.YEAR, 0);
        cale.set(Calendar.DAY_OF_YEAR, 1);
        firstday = format.format(cale.getTime());
        return firstday;
    }

    public static Date getWeekStartDate() {
        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        Date date = cal.getTime();
        return date;
    }

    public String getPreviousWeekSunday() {

        int weeks = -1;

        int mondayPlus = getMondayPlus();

        GregorianCalendar currentDate = new GregorianCalendar();

        currentDate.add(5, mondayPlus + weeks);


        Date monday = currentDate.getTime();

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String preMonday = sdf.format(monday);

        //System.out.println("上周最后一天："+CommonUtils.parseDateToLong(preMonday+" 23:59:59", "yyyy-MM-dd hh:mm:ss"));

        return preMonday;
    }

    public static int getMondayPlus() {
        Calendar cd = Calendar.getInstance();

        int dayOfWeek = cd.get(7) - 1;

        if (dayOfWeek == 1) {
            return 0;
        }

        return (1 - dayOfWeek);
    }

    public String getPreviousMonday() {
        int week = -1;

        int mondayPlus = getMondayPlus();

        GregorianCalendar currentDate = new GregorianCalendar();

        currentDate.add(5, mondayPlus + 7 * week);

        Date monday = currentDate.getTime();


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        String preMonday = sdf.format(monday);

        //System.out.println("上周第一天："+CommonUtils.parseDateToLong(preMonday+" 00:00:00", "yyyy-MM-dd hh:mm:ss"));
        return preMonday;
    }

    public static void main(String[] args) throws Exception {
        String previousWeekday = new UtilController().getPreviousWeekSunday();
        System.out.println(yearFirstDay());
    }

}
