package com.st.power.controller;

import com.st.power.mapper.OrganizationMapper;
import com.st.power.pojo.common.AjaxJson;
import com.st.power.pojo.St3Level;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("org")
public class OrganizationController {

    @Autowired
    OrganizationMapper organizationMapper;

    @RequestMapping("saveLevel")
    public AjaxJson<String> saveLevel(St3Level level) {
        level.setType(level.getType() + 10);
        organizationMapper.saveLevel(level);
        return new AjaxJson<String>();
    }

    @RequestMapping("getOrg")
    public AjaxJson<St3Level> getOrg() {
        AjaxJson<St3Level> ajaxJson = new AjaxJson<>();
//        List<St3Level> list=
        List<Long> ids = organizationMapper.getIds();

        List<St3Level> list = organizationMapper.getLevel();
        St3Level st3Level = new St3Level();
        st3Level.setType(0);
        st3Level.setId(-1);
        st3Level.setName("根");
        st3Level.setChildren(list);
        List<St3Level> list1 = new ArrayList<>();
        list1.add(st3Level);
        ajaxJson.list = list1;

        ids.add(-1L);
        ajaxJson.extraData = ids;
        return ajaxJson;
    }

    @RequestMapping("getFactory")
    public AjaxJson getFactory() {
        AjaxJson<St3Level> ajaxJson = new AjaxJson<>();
        ajaxJson.list = organizationMapper.getFactory();
        return ajaxJson;
    }

    @RequestMapping("roomOrGroup")
    public AjaxJson<St3Level> roomOrGroup(Integer id, String test) {
        System.out.println(test);
        AjaxJson<St3Level> ajaxJson = new AjaxJson<>();
        ajaxJson.list = organizationMapper.roomOrGroup(id);
        return ajaxJson;
    }
}
