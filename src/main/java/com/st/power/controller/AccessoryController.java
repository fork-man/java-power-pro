package com.st.power.controller;

import com.st.power.mapper.AccessoryMapper;
import com.st.power.pojo.AccessoryCategoryPojo;
import com.st.power.pojo.AccessoryPojo;
import com.st.power.pojo.common.AjaxJson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("accessory")
public class AccessoryController {
    @Autowired
    AccessoryMapper accessoryMapper;

    @RequestMapping("accessoryList")
    public AjaxJson accessoryList(AccessoryPojo accessoryPojo) {
        AjaxJson<AccessoryPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = accessoryMapper.getAccessoryList(accessoryPojo);
        Long count = accessoryMapper.accessoryCount();
        ajaxJson.count = count.intValue();
        return ajaxJson;
    }

    @RequestMapping("saveAccessory")
    public AjaxJson saveAccessory(AccessoryPojo accessoryPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        accessoryMapper.saveAccessory(accessoryPojo);
        return ajaxJson;
    }

    @RequestMapping("updateAccessory")
    public AjaxJson updateAccessory(AccessoryPojo accessoryPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        accessoryMapper.updateAccessory(accessoryPojo);
        return ajaxJson;
    }

    //-----------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping("accessoryCategoryList")
    public AjaxJson accessoryCategoryList() {
        AjaxJson<AccessoryCategoryPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = accessoryMapper.accessoryCategoryList();
        return ajaxJson;
    }

    @RequestMapping("saveAccessoryCategory")
    public AjaxJson saveAccessoryCategory(AccessoryCategoryPojo accessoryCategoryPojo) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        accessoryMapper.saveAccessoryCategory(accessoryCategoryPojo);
        return ajaxJson;
    }

    //----------------------------------------------------------------------------------------------------------------------------------------------
    @RequestMapping("relationship")
    public AjaxJson relationship(Long id) {
        AjaxJson<AccessoryPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = accessoryMapper.getRelationship(id);
        return ajaxJson;
    }

    @RequestMapping("accessoryNotAddList")
    public AjaxJson accessoryNotAddList(Long id) {
        AjaxJson<AccessoryPojo> ajaxJson = new AjaxJson<>();
        ajaxJson.list = accessoryMapper.accessoryNotAddList(id);
        return ajaxJson;
    }

    @RequestMapping("saveRelationship")
    public AjaxJson saveRelationship(Long deviceId, String accessoryIdArr) {
        AjaxJson<String> ajaxJson = new AjaxJson<>();
        String[] arr = accessoryIdArr.split(",");
        List<AccessoryController.Relationship> list = new ArrayList<>();
        for (String id : arr) {
            Relationship relationship = new Relationship();
            relationship.deviceId = deviceId;
            relationship.accessoryId = Long.valueOf(id);
            list.add(relationship);
        }
        accessoryMapper.saveRelationship(list);
        return ajaxJson;
    }

    public static class Relationship {
        public Long deviceId;
        public Long accessoryId;
    }

}
