package com.st.power.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("download")
@Slf4j
public class DownloadController {
    @RequestMapping("index")
    public String index(String path) {
        log.info("进入");
        return "download-temp";
    }
}
